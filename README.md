# Calendar-QA

### Master Branch
![Master build status](https://gitlab.com/ivanph1017/calendar-qa/badges/master/pipeline.svg)
![Master coverage report](https://gitlab.com/ivanph1017/calendar-qa/badges/master/coverage.svg?job=unitTestsRelease)

### Develop Branch
![Develop build status](https://gitlab.com/ivanph1017/calendar-qa/badges/develop/pipeline.svg)
![Develop coverage report](https://gitlab.com/ivanph1017/calendar-qa/badges/develop/coverage.svg?job=unitTestsDebug)

## Product Description
Calendar-QA is an Android app that allow adding new events to the user's Google Calendar.  
The user flow describes 3 steps:  
- Event Configuration    
- Adding a new Event    
- Finishing message  

![Happy user flow](https://gitlab.com/ivanph1017/calendar-qa/raw/develop/images/happy_flow.jpg)

Every step has its own user story and its UX/UI design according to the scenarios' scope.

## Deliveries
- Sprint 1: Happy user flow is sent to the master branch at the end of the sprint.  
- Sprint 2: Unit testing and UI testings are added to the master branch at the end of the sprint.  

## Acceptance criterias

## Functional requirements
- Functional criterias of the main features are described in their user stories.

### Non-functional requirements
- Usability: Every screen is developed according to the UX/UI prototype. This one was designeed following the Material Design guidelines promoted by Google.  
- Interoperability: The one and only external service requested is the Google Calendar API from user's Google account.  
- Portability: The app is only for Android. However, the target version is Android 9 and has backwards support until Android 5 as minimum version.  
- Performance: The app is developed written in Java on Android native platform assuring the best performance.
- Availability: The app is available only by wired installation on the device.
- Maintenability: The develop process assures a high quality code following Java code styling with unit testing and UI testing. The documentation includes the user flow described in the user stories and UI/UX designs. Also, the code may be documented, but it is not mandatory. In addition, the supporting time ends at the end of the sprint 2.

## Exclusions
- Usability: Accesability features are not part of the app's scope.
- Security: Ways of avoiding app hijacking are not part of the app's scope.  
- Extensibility: The only functional use case of the app is adding new events to the user's Google calendar. Thus, extending features for updating existing events, displaying created events and deleting any event are not part of the app's scope.  

