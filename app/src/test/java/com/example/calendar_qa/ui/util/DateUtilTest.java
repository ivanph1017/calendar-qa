package com.example.calendar_qa.ui.util;

import org.junit.Test;

import java.text.ParseException;
import java.util.Calendar;

import static java.util.Calendar.HOUR_OF_DAY;
import static junit.framework.TestCase.assertEquals;

public class DateUtilTest {
    private static final transient int YEAR = 2019;
    private static final transient int MONTH = 7;
    private static final transient int DAY = 15;
    private static final transient String EXPECTED_DATE = "15/08/2019";

    private static final transient int HOUR = 21;
    private static final transient int MINUTE = 59;
    private static final transient String EXPECTED_TIME = "09:59 PM";


    @Test
    public void testGetFormattedDate() {
        assertEquals(EXPECTED_DATE, DateUtil.getFormattedDate(YEAR,MONTH,DAY));
    }

    @Test
    public void testGetFormattedTime() {
        assertEquals(EXPECTED_TIME, DateUtil.getFormattedTime(HOUR,MINUTE));
    }

    @Test
    public void testParseDateTime() throws ParseException {

        final String date = DateUtil.getFormattedDate(YEAR, MONTH, DAY);
        final String time = DateUtil.getFormattedTime(HOUR_OF_DAY, MINUTE);
        final Calendar calendarToTest = DateUtil.parseDateTime(date,time);

        assertEquals(YEAR, calendarToTest.get(Calendar.YEAR));
        assertEquals(MONTH, calendarToTest.get(Calendar.MONTH));
        assertEquals(DAY, calendarToTest.get(Calendar.DAY_OF_MONTH));

        assertEquals(HOUR_OF_DAY, calendarToTest.get(HOUR_OF_DAY));
        assertEquals(MINUTE, calendarToTest.get(Calendar.MINUTE));
    }
}