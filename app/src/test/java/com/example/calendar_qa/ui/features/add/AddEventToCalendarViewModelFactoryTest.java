package com.example.calendar_qa.ui.features.add;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AddEventToCalendarViewModelFactoryTest {

    @Test
    public void testCreate() {
        assertThat(AddEventToCalendarViewModelFactory.getInstance().create(AddEventToCalendarViewModel.class),
                is(instanceOf(AddEventToCalendarViewModel.class)));
    }
}