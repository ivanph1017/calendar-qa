package com.example.calendar_qa.ui.features.add;

import android.widget.DatePicker;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DatePickerFragmentTest {

    private static final transient int YEAR = 2019;
    private static final transient int MONTH = 12;
    private static final transient int DAY = 5;
    private static final transient int INVALID_DATE_TYPE = -1;

    @Mock
    private transient DatePicker datePicker;

    private transient DatePickerFragment datePickerFragment;

    @Test
    public void testOnStartDateSet() {
        datePickerFragment = new DatePickerFragment(AddEventToCalendarConstant.START_DATE_EVENT);
        datePickerFragment.onDateSet(datePicker, YEAR, MONTH, DAY);
    }

    @Test
    public void testOnEndDateSet() {
        datePickerFragment = new DatePickerFragment(AddEventToCalendarConstant.END_DATE_EVENT);
        datePickerFragment.onDateSet(datePicker, YEAR, MONTH, DAY);
    }

    @Test
    public void testOnInvalidDateType() {
        datePickerFragment = new DatePickerFragment(INVALID_DATE_TYPE);
        datePickerFragment.onDateSet(datePicker, YEAR, MONTH, DAY);
    }
}