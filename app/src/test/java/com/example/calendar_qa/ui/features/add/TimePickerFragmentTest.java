package com.example.calendar_qa.ui.features.add;

import android.widget.TimePicker;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TimePickerFragmentTest {

    private static final transient int HOUR_OF_DAY = 15;
    private static final transient int MINUTE = 30;
    private static final transient int INVALID_TIME_TYPE = -1;

    @Mock
    private transient TimePicker timePicker;

    private TimePickerFragment timePickerFragment;

    @Test
    public void testOnStartTimeSet() {
        timePickerFragment = new TimePickerFragment(AddEventToCalendarConstant.START_TIME_EVENT);
        timePickerFragment.onTimeSet(timePicker, HOUR_OF_DAY, MINUTE);
    }

    @Test
    public void testOnEndTimeSet() {
        timePickerFragment = new TimePickerFragment(AddEventToCalendarConstant.END_TIME_EVENT);
        timePickerFragment.onTimeSet(timePicker, HOUR_OF_DAY, MINUTE);
    }

    @Test
    public void testOnInvalidTimeType() {
        timePickerFragment = new TimePickerFragment(INVALID_TIME_TYPE);
        timePickerFragment.onTimeSet(timePicker, HOUR_OF_DAY, MINUTE);
    }
}