package com.example.calendar_qa.ui.features.add;

import android.text.Editable;

import com.example.calendar_qa.R;
import com.google.android.material.textfield.TextInputLayout;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@RunWith(MockitoJUnitRunner.class)
public class TextListenerTest {

    private static final transient int ZERO_CHARACTERS = 0;
    private static final transient int NINE_CHARACTERS = 9;

    private transient TextListener textListener;

    @Mock
    private transient TextInputLayout textInputLayout;
    @Mock
    private transient TextListener.Listener listener;
    @Mock
    private transient Editable editable;

    @Before
    public void setUp() {
        textListener = new TextListener(textInputLayout, listener);
    }

    @Test
    public void testAfterTextChangedEmptyEditable() {
        given(editable.length()).willReturn(ZERO_CHARACTERS);
        textListener.afterTextChanged(editable);
        then(listener).should().onTextInputLayoutError(textInputLayout, R.string.field_empty_error_message);
        then(listener).should().hideKeyword();
        then(listener).should().validateEnablingButton();
    }

    @Test
    public void testAfterTextChangedExceedMaxCounter() {
        final int maxLenght = 5;
        given(editable.length()).willReturn(NINE_CHARACTERS);
        given(textInputLayout.isCounterEnabled()).willReturn(true);
        given(textInputLayout.getCounterMaxLength()).willReturn(maxLenght);
        textListener.afterTextChanged(editable);
        then(listener).should().onTextInputLayoutError(textInputLayout, R.string.field_max_characters_error_message);
        then(listener).should().validateEnablingButton();
    }

    @Test
    public void testAfterTextChangedLessThanMaxCounter() {
        final int maxLenght = 10;
        given(editable.length()).willReturn(NINE_CHARACTERS);
        given(textInputLayout.isCounterEnabled()).willReturn(true);
        given(textInputLayout.getCounterMaxLength()).willReturn(maxLenght);
        textListener.afterTextChanged(editable);
        then(textInputLayout).should().setErrorEnabled(false);
        then(textInputLayout).should().setEndIconDrawable(R.drawable.ic_check);
        then(listener).should().validateEnablingButton();
    }

    @Test
    public void testAfterTextChangedWithoutCounter() {
        given(editable.length()).willReturn(NINE_CHARACTERS);
        given(textInputLayout.isCounterEnabled()).willReturn(false);
        textListener.afterTextChanged(editable);
        then(textInputLayout).should().setErrorEnabled(false);
        then(textInputLayout).should().setEndIconDrawable(R.drawable.ic_check);
        then(listener).should().validateEnablingButton();
    }
}