package com.example.calendar_qa.ui.features.add;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import com.example.calendar_qa.R;
import com.example.calendar_qa.ui.util.DateUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(MockitoJUnitRunner.class)
public class AddEventToCalendarViewModelTest {

    private static final transient int YEAR = 2019;
    private static final transient int MONTH = 12;
    private static final transient int DAY = 5;
    private static final transient int HOUR_OF_DAY = 15;

    private static final transient int START_MINUTE = 30;
    private static final transient int END_MINUTE = 31;

    private static final transient int INVALID_END_MINUTE = 29;

    private transient AddEventToCalendarViewModel viewModel;

    @Rule
    public transient InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    @Mock
    private transient AddEventToCalendarModel model;
    @Mock
    private transient Observer<AddEventToCalendarModel> dataObserver;
    @Mock
    private transient Observer<AddEventToCalendarModel> askPermissionObserver;
    @Mock
    private transient Observer<Integer> errorMessageResObserver;

    @Before
    public void setUp() {
        viewModel = new AddEventToCalendarViewModel();
    }

    @After
    public void clean() {
        viewModel.getLiveData().removeObserver(dataObserver);
        viewModel.getAskPermission().removeObserver(askPermissionObserver);
        viewModel.getErrorMessageRes().removeObserver(errorMessageResObserver);
    }

    @Test
    public void testSetStartDate() {
        given(model.getStartDate()).willReturn(DateUtil.getFormattedDate(YEAR, MONTH, DAY));
        given(model.getEndDate()).willReturn(null);
        viewModel.getLiveData().observeForever(dataObserver);
        viewModel.setDate(AddEventToCalendarConstant.START_DATE_EVENT, YEAR, MONTH, DAY);
        if (viewModel.getLiveData().getValue() != null) {
            assertEquals(model.getStartDate(), viewModel.getLiveData().getValue().getStartDate());
            assertEquals(model.getEndDate(), viewModel.getLiveData().getValue().getEndDate());
        }
    }

    @Test
    public void testSetStartTime() {
        given(model.getStartTime()).willReturn(DateUtil.getFormattedTime(HOUR_OF_DAY, START_MINUTE));
        given(model.getEndTime()).willReturn(null);
        viewModel.getLiveData().observeForever(dataObserver);
        viewModel.setTime(AddEventToCalendarConstant.START_TIME_EVENT, HOUR_OF_DAY, START_MINUTE);
        if (viewModel.getLiveData().getValue() != null) {
            assertEquals(model.getStartTime(), viewModel.getLiveData().getValue().getStartTime());
            assertEquals(model.getEndTime(), viewModel.getLiveData().getValue().getEndTime());
        }
    }

    @Test
    public void testSetEndDate() {
        given(model.getStartDate()).willReturn(null);
        given(model.getEndDate()).willReturn(DateUtil.getFormattedDate(YEAR, MONTH, DAY));
        viewModel.getLiveData().observeForever(dataObserver);
        viewModel.setDate(AddEventToCalendarConstant.END_DATE_EVENT, YEAR, MONTH, DAY);
        if (viewModel.getLiveData().getValue() != null) {
            assertEquals(model.getStartDate(), viewModel.getLiveData().getValue().getStartDate());
            assertEquals(model.getEndDate(), viewModel.getLiveData().getValue().getEndDate());
        }
    }

    @Test
    public void testSetEndTime() {
        given(model.getStartTime()).willReturn(null);
        given(model.getEndTime()).willReturn(DateUtil.getFormattedTime(HOUR_OF_DAY, END_MINUTE));
        viewModel.getLiveData().observeForever(dataObserver);
        viewModel.setTime(AddEventToCalendarConstant.END_TIME_EVENT, HOUR_OF_DAY, END_MINUTE);
        if (viewModel.getLiveData().getValue() != null) {
            assertEquals(model.getStartTime(), viewModel.getLiveData().getValue().getStartTime());
            assertEquals(model.getEndTime(), viewModel.getLiveData().getValue().getEndTime());
        }
    }

    @Test
    public void testSetInvalidDateType() {
        given(model.getStartDate()).willReturn(null);
        given(model.getEndDate()).willReturn(null);
        viewModel.getLiveData().observeForever(dataObserver);
        viewModel.setDate(anyInt(), YEAR, MONTH, DAY);
        if (viewModel.getLiveData().getValue() != null) {
            assertEquals(model.getStartDate(), viewModel.getLiveData().getValue().getStartDate());
            assertEquals(model.getEndDate(), viewModel.getLiveData().getValue().getEndDate());
        }
    }

    @Test
    public void testSetInvalidTimeType() {
        given(model.getStartTime()).willReturn(null);
        given(model.getEndTime()).willReturn(null);
        viewModel.getLiveData().observeForever(dataObserver);
        viewModel.setTime(anyInt(), HOUR_OF_DAY, END_MINUTE);
        if (viewModel.getLiveData().getValue() != null) {
            assertEquals(model.getStartTime(), viewModel.getLiveData().getValue().getStartTime());
            assertEquals(model.getEndTime(), viewModel.getLiveData().getValue().getEndTime());
        }
    }

    @Test
    public void testAddEventWithValidDateTimes() {
        given(model.getStartDate()).willReturn(DateUtil.getFormattedDate(YEAR, MONTH, DAY));
        given(model.getStartTime()).willReturn(DateUtil.getFormattedTime(HOUR_OF_DAY, START_MINUTE));
        given(model.getEndDate()).willReturn(DateUtil.getFormattedDate(YEAR, MONTH, DAY));
        given(model.getEndTime()).willReturn(DateUtil.getFormattedTime(HOUR_OF_DAY, END_MINUTE));
        setValidDatesTimes();
        viewModel.setTime(AddEventToCalendarConstant.END_TIME_EVENT, HOUR_OF_DAY, END_MINUTE);
        viewModel.getAskPermission().observeForever(askPermissionObserver);
        viewModel.addEvent();
        if (viewModel.getAskPermission().getValue() != null) {
            assertEquals(model.getStartDate(), viewModel.getAskPermission().getValue().getStartDate());
            assertEquals(model.getStartTime(), viewModel.getAskPermission().getValue().getStartTime());
            assertEquals(model.getEndDate(), viewModel.getAskPermission().getValue().getEndDate());
            assertEquals(model.getEndTime(), viewModel.getAskPermission().getValue().getEndTime());
        }
    }

    @Test
    public void testAddEventWithInValidDateTimes() {
        setValidDatesTimes();
        viewModel.setTime(AddEventToCalendarConstant.END_TIME_EVENT, HOUR_OF_DAY, INVALID_END_MINUTE);
        viewModel.getErrorMessageRes().observeForever(errorMessageResObserver);
        viewModel.addEvent();
        if (viewModel.getErrorMessageRes().getValue() != null) {
            assertEquals(Long.valueOf(R.string.invalid_dates_error_message),
                    Long.valueOf(viewModel.getErrorMessageRes().getValue()));
        }
        assertNull(viewModel.getAskPermission().getValue());
    }

    private void setValidDatesTimes() {
        viewModel.setDate(AddEventToCalendarConstant.START_DATE_EVENT, YEAR, MONTH, DAY);
        viewModel.setTime(AddEventToCalendarConstant.START_TIME_EVENT, HOUR_OF_DAY, START_MINUTE);
        viewModel.setDate(AddEventToCalendarConstant.END_DATE_EVENT, YEAR, MONTH, DAY);
    }
}