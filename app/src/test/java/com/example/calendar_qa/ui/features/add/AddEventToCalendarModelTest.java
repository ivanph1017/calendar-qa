package com.example.calendar_qa.ui.features.add;

import com.example.calendar_qa.ui.util.DateUtil;

import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class AddEventToCalendarModelTest {

    private static final transient int YEAR = 2019;
    private static final transient int MONTH = 12;
    private static final transient int DAY = 5;
    private static final transient int HOUR_OF_DAY = 15;

    private static final transient int START_MINUTE = 30;
    private static final transient int END_MINUTE = 31;

    private static final transient int INVALID_END_MINUTE = 29;
    private static final transient int INVALID_TYPE = -1;

    private static final transient String TEXT = "ABCEDFGDFGGG4343";
    private static final transient String MINUTES_IN_STRING = "15";

    private transient AddEventToCalendarModel model;

    @Before
    public void setUp() {
        model = new AddEventToCalendarModel();
    }

    @Test
    public void testSetStartDate() {
        model.setDate(AddEventToCalendarConstant.START_DATE_EVENT, YEAR, MONTH, DAY);
        assertEquals(DateUtil.getFormattedDate(YEAR, MONTH, DAY), model.getStartDate());
        assertNull(model.getEndDate());
    }

    @Test
    public void testSetStartTime() {
        model.setTime(AddEventToCalendarConstant.START_TIME_EVENT, HOUR_OF_DAY, START_MINUTE);
        assertEquals(DateUtil.getFormattedTime(HOUR_OF_DAY, START_MINUTE), model.getStartTime());
        assertNull(model.getEndTime());
    }

    @Test
    public void testSetEndDate() {
        model.setDate(AddEventToCalendarConstant.END_DATE_EVENT, YEAR, MONTH, DAY);
        assertNull(model.getStartDate());
        assertEquals(DateUtil.getFormattedDate(YEAR, MONTH, DAY), model.getEndDate());
    }

    @Test
    public void testSetEndTime() {
        model.setTime(AddEventToCalendarConstant.END_TIME_EVENT, HOUR_OF_DAY, START_MINUTE);
        assertNull(model.getStartTime());
        assertEquals(DateUtil.getFormattedTime(HOUR_OF_DAY, START_MINUTE), model.getEndTime());
    }

    @Test
    public void testSetInvalidDateType() {
        model.setDate(INVALID_TYPE, YEAR, MONTH, DAY);
        assertNull(model.getStartDate());
        assertNull(model.getEndDate());
    }

    @Test
    public void testSetInvalidTimeType() {
        model.setTime(INVALID_TYPE, HOUR_OF_DAY, END_MINUTE);
        assertNull(model.getStartTime());
        assertNull(model.getEndTime());
    }

    @Test
    public void testHasValidDateTimes() throws ParseException {
        setValidDatesTimes();
        model.setTime(AddEventToCalendarConstant.END_TIME_EVENT, HOUR_OF_DAY, END_MINUTE);
        assertTrue(model.hasValidDateTimes());
        assertEquals(DateUtil.parseDateTime(model.getStartDate(), model.getStartTime()), model.getStartDateTime());
        assertEquals(DateUtil.parseDateTime(model.getEndDate(), model.getEndTime()), model.getEndDateTime());
    }

    @Test
    public void testHasInvalidDateTimes() throws ParseException {
        setValidDatesTimes();
        model.setTime(AddEventToCalendarConstant.END_TIME_EVENT, HOUR_OF_DAY, INVALID_END_MINUTE);
        assertFalse(model.hasValidDateTimes());
        assertEquals(DateUtil.parseDateTime(model.getStartDate(), model.getStartTime()), model.getStartDateTime());
        assertEquals(DateUtil.parseDateTime(model.getEndDate(), model.getEndTime()), model.getEndDateTime());
    }

    @Test(expected = ParseException.class)
    public void testHasInvalidDateTimesCannotParse() throws ParseException {
        setValidDatesTimes();
        model.setTime(INVALID_TYPE, HOUR_OF_DAY, END_MINUTE);
        model.hasValidDateTimes();
    }

    private void setValidDatesTimes() {
        model.setDate(AddEventToCalendarConstant.START_DATE_EVENT, YEAR, MONTH, DAY);
        model.setTime(AddEventToCalendarConstant.START_TIME_EVENT, HOUR_OF_DAY, START_MINUTE);
        model.setDate(AddEventToCalendarConstant.END_DATE_EVENT, YEAR, MONTH, DAY);
    }

    @Test
    public void testClone() throws ParseException, CloneNotSupportedException {
        setValidDatesTimes();
        model.setTime(AddEventToCalendarConstant.END_TIME_EVENT, HOUR_OF_DAY, END_MINUTE);
        model.hasValidDateTimes();
        assertEquals(model, model.clone());
    }

    @Test
    public void testSetTitle() {
        model.setTitle(TEXT);
        assertEquals(TEXT, model.getTitle());
    }

    @Test
    public void testSetLocation() {
        model.setLocation(TEXT);
        assertEquals(TEXT, model.getLocation());
    }

    @Test
    public void testSetDescription() {
        model.setDescription(TEXT);
        assertEquals(TEXT, model.getDescription());
    }

    @Test
    public void testSetReminderText() {
        model.setMinutesReminderText(MINUTES_IN_STRING);
        assertEquals(MINUTES_IN_STRING, model.getMinutesReminderText());
    }

    @Test
    public void testGetReminderWithoutMinutes() {
        model.setMinutesReminderText(TEXT);
        assertEquals(0, model.getMinutesReminder());
    }

    @Test
    public void testGetReminderWithMinutes() {
        model.setMinutesReminderText(MINUTES_IN_STRING);
        assertEquals(Integer.parseInt(MINUTES_IN_STRING), model.getMinutesReminder());
    }
}