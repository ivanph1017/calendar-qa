package com.example.calendar_qa.ui.util;

final class DateConstant {

    static final transient String LOCALE = "es_PE";
    static final transient String DATE_FORMAT = "dd/MM/yyyy";
    static final transient String TIME_FORMAT = "hh:mm aa";
    static final transient String DATE_TIME_TEMPLATE = "1$%s 2$%s";
    static final transient String DATE_TIME_FORMAT = String.format(DATE_TIME_TEMPLATE, DATE_FORMAT, TIME_FORMAT);

    private DateConstant() throws IllegalAccessException {
        throw new IllegalAccessException("This is a utility class");
    }
}
