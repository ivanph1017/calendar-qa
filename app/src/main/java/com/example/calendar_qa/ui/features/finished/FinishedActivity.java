package com.example.calendar_qa.ui.features.finished;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.databinding.DataBindingUtil;

import com.example.calendar_qa.R;
import com.example.calendar_qa.databinding.ActivityFinishedBinding;

public class FinishedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActivityFinishedBinding binding =
                DataBindingUtil.setContentView(this, R.layout.activity_finished);
        binding.btnBackToHome.setOnClickListener(view -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        NavUtils.navigateUpFromSameTask(this);
    }
}
