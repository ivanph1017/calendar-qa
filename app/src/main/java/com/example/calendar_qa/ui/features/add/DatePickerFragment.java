package com.example.calendar_qa.ui.features.add;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private final transient int dateType;

    DatePickerFragment(final int dateType) {
        this.dateType = dateType;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Create a new instance of DatePickerDialog and return it
        if (getActivity() != null) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            final int year = c.get(Calendar.YEAR);
            final int month = c.get(Calendar.MONTH);
            final int day = c.get(Calendar.DAY_OF_MONTH);
            final DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMinDate(System.currentTimeMillis());
            return dialog;
        }
        throw new InstantiationException("Activity is null", null);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        if (getActivity() != null) {
            final AddEventToCalendarViewModel viewModel = ViewModelProviders.of(getActivity(),
                    AddEventToCalendarViewModelFactory.getInstance()).get(AddEventToCalendarViewModel.class);
            viewModel.setDate(dateType, year, month, day);
        }
    }
}
