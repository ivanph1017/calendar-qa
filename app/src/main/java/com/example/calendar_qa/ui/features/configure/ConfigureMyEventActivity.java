package com.example.calendar_qa.ui.features.configure;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;

import com.example.calendar_qa.R;
import com.example.calendar_qa.ui.features.add.AddEventToCalendarActivity;
import com.example.calendar_qa.databinding.ActivityConfigureMyEventBinding;

public class ConfigureMyEventActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActivityConfigureMyEventBinding binding =
                DataBindingUtil.setContentView(this, R.layout.activity_configure_my_event);
        binding.btnConfigure.setOnClickListener(view ->
                startActivity(new Intent(this, AddEventToCalendarActivity.class)));
    }
}
