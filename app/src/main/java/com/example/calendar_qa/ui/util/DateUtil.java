package com.example.calendar_qa.ui.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public final class DateUtil {

    private DateUtil() throws IllegalAccessException {
        throw new IllegalAccessException("This is a utility class");
    }

    public static String getFormattedDate(final int year, final int month, final int day) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        final SimpleDateFormat formatter = new SimpleDateFormat(DateConstant.DATE_FORMAT,
                new Locale(DateConstant.LOCALE));
        return formatter.format(calendar.getTime());
    }

    public static String getFormattedTime(final int hourOfDay, final int minute) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, hourOfDay, minute);
        final SimpleDateFormat formatter = new SimpleDateFormat(DateConstant.TIME_FORMAT,
                new Locale(DateConstant.LOCALE));
        return formatter.format(calendar.getTime());
    }

    public static Calendar parseDateTime(final String date, final String time) throws ParseException {
        final SimpleDateFormat formatter = new SimpleDateFormat(DateConstant.DATE_TIME_FORMAT,
                new Locale(DateConstant.LOCALE));
        final String dateTime = String.format(DateConstant.DATE_TIME_TEMPLATE, date, time);
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(formatter.parse(dateTime));
        return calendar;
    }
}
