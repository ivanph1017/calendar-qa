package com.example.calendar_qa.ui.features.add;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class AddEventToCalendarViewModelFactory implements ViewModelProvider.Factory {

    private AddEventToCalendarViewModelFactory() {
        // empty constructor required
    }

    private static class LazyHolder {
        static final AddEventToCalendarViewModelFactory INSTANCE = new AddEventToCalendarViewModelFactory();
    }

    static AddEventToCalendarViewModelFactory getInstance() {
        return LazyHolder.INSTANCE;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(AddEventToCalendarViewModel.class)) {
            return (T) new AddEventToCalendarViewModel();
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
