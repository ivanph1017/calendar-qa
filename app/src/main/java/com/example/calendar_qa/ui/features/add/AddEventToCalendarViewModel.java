package com.example.calendar_qa.ui.features.add;

import android.util.Pair;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.calendar_qa.R;

import java.text.ParseException;

public class AddEventToCalendarViewModel extends ViewModel {

    private final transient AddEventToCalendarModel model;
    private final transient MutableLiveData<AddEventToCalendarModel> liveData;
    private final transient MutableLiveData<Integer> errorMessageRes;
    private final transient MutableLiveData<Pair<Integer, Exception>> errorMessageResException;
    private final transient MutableLiveData<AddEventToCalendarModel> askPermission;

    public AddEventToCalendarViewModel() {
        this.model = new AddEventToCalendarModel();
        this.liveData = new MutableLiveData<>();
        this.errorMessageRes = new MutableLiveData<>();
        this.errorMessageResException = new MutableLiveData<>();
        this.askPermission = new MutableLiveData<>();
        liveData.postValue(model);
    }

    public LiveData<AddEventToCalendarModel> getLiveData() {
        return liveData;
    }

    LiveData<Integer> getErrorMessageRes() {
        return errorMessageRes;
    }

    LiveData<Pair<Integer, Exception>> getErrorMessageResException() {
        return errorMessageResException;
    }

    LiveData<AddEventToCalendarModel> getAskPermission() {
        return askPermission;
    }

    void setDate(final int type, final int year, final int month, final int day) {
        model.setDate(type, year, month, day);
        liveData.postValue(model);
    }

    void setTime(final int type, final int hourOfDay, final int minute) {
        model.setTime(type, hourOfDay, minute);
        liveData.postValue(model);
    }

    public void addEvent() {
        try {
            if (model.hasValidDateTimes()) {
                askPermission.postValue(model.clone());
            } else {
                errorMessageRes.postValue(R.string.invalid_dates_error_message);
            }
        } catch (ParseException exception) {
            errorMessageResException.postValue(new Pair<>(R.string.date_time_poorly_written, exception));
        } catch (CloneNotSupportedException exception) {
            errorMessageResException.postValue(new Pair<>(R.string.input_data_could_not_processed, exception));
        }
    }
}
