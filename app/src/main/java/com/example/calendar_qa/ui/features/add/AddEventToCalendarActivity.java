package com.example.calendar_qa.ui.features.add;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.calendar_qa.R;
import com.example.calendar_qa.databinding.ActivityAddEventToCalendarBinding;
import com.example.calendar_qa.device.CalendarHandler;
import com.example.calendar_qa.device.OnCompleteListener;
import com.example.calendar_qa.ui.features.finished.FinishedActivity;
import com.google.android.material.textfield.TextInputLayout;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class AddEventToCalendarActivity extends AppCompatActivity
        implements OnCompleteListener, TextListener.Listener {

    private static final transient String[] READ_WRITE_CALENDAR =
            {Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR};
    private static final transient int READ_WRITE_CALENDAR_PERM = 100;

    private transient ActivityAddEventToCalendarBinding binding;
    private transient AddEventToCalendarViewModel viewModel;
    private transient CalendarHandler calendarHandler;
    private transient AddEventToCalendarModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, AddEventToCalendarViewModelFactory.getInstance())
                .get(AddEventToCalendarViewModel.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_event_to_calendar);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.dropdown_menu_popup_item,
                getResources().getStringArray(R.array.minutes));
        binding.actvReminder.setAdapter(adapter);
        setUpTextChangedListeners();
        setUpOnFocusListeners();
        hideSoftInputOnFocus();
        validateEnablingButton();
        setUpObservers();
        calendarHandler = new CalendarHandler(getContentResolver(), this);
    }

    private void setUpTextChangedListeners() {
        binding.etTitle.addTextChangedListener(new TextListener(binding.layoutTitle, this));
        binding.etLocation.addTextChangedListener(new TextListener(binding.layoutLocation, this));
        binding.etDescription.addTextChangedListener(new TextListener(binding.layoutDescription, this));
        binding.etStartDate.addTextChangedListener(new TextListener(binding.layoutStartDate, this));
        binding.etStartTime.addTextChangedListener(new TextListener(binding.layoutStartTime, this));
        binding.etEndDate.addTextChangedListener(new TextListener(binding.layoutEndDate, this));
        binding.etEndTime.addTextChangedListener(new TextListener(binding.layoutEndTime, this));
    }

    private void setUpOnFocusListeners() {
        binding.etStartDate.setOnFocusChangeListener((view, hasFocus) ->
                showDatePickerDialog(hasFocus, AddEventToCalendarConstant.START_DATE_EVENT));
        binding.etStartTime.setOnFocusChangeListener((view, hasFocus) ->
                showTimePickerDialog(hasFocus, AddEventToCalendarConstant.START_TIME_EVENT));
        binding.etEndDate.setOnFocusChangeListener((view, hasFocus) ->
                showDatePickerDialog(hasFocus, AddEventToCalendarConstant.END_DATE_EVENT));
        binding.etEndTime.setOnFocusChangeListener((view, hasFocus) ->
                showTimePickerDialog(hasFocus, AddEventToCalendarConstant.END_TIME_EVENT));
    }

    private void hideSoftInputOnFocus() {
        binding.etStartDate.setShowSoftInputOnFocus(false);
        binding.etStartTime.setShowSoftInputOnFocus(false);
        binding.etEndDate.setShowSoftInputOnFocus(false);
        binding.etEndTime.setShowSoftInputOnFocus(false);
        binding.actvReminder.setShowSoftInputOnFocus(false);
    }

    @Override
    public void validateEnablingButton() {
        binding.btnAdd.setEnabled(areFilledFields() && areValidFieldLengths());
    }

    @Override
    public void onTextInputLayoutError(final TextInputLayout textInputLayout, final int errorMessageRes) {
        textInputLayout.setErrorEnabled(true);
        textInputLayout.setError(getString(errorMessageRes));
        textInputLayout.setEndIconDrawable(R.drawable.ic_exclamation);
    }

    @Override
    public void hideKeyword() {
        if (getCurrentFocus() != null) {
            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private boolean areFilledFields() {
        return !TextUtils.isEmpty(binding.etTitle.getText())
                && !TextUtils.isEmpty(binding.etLocation.getText())
                && !TextUtils.isEmpty(binding.etDescription.getText())
                && !TextUtils.isEmpty(binding.etStartDate.getText())
                && !TextUtils.isEmpty(binding.etStartTime.getText())
                && !TextUtils.isEmpty(binding.etEndDate.getText())
                && !TextUtils.isEmpty(binding.etEndTime.getText());
    }

    private boolean areValidFieldLengths() {
        return binding.etTitle.getText() != null
                && binding.layoutTitle.isCounterEnabled()
                && binding.etTitle.getText().length() <= binding.layoutTitle.getCounterMaxLength()
                && binding.etLocation.getText() != null
                && binding.layoutLocation.isCounterEnabled()
                && binding.etLocation.getText().length() <= binding.layoutLocation.getCounterMaxLength()
                && binding.etDescription.getText() != null
                && binding.layoutDescription.isCounterEnabled()
                && binding.etDescription.getText().length() <= binding.layoutDescription.getCounterMaxLength();
    }

    private void showDatePickerDialog(final boolean hasFocus, final int dateEvent) {
        if (hasFocus) {
            final DialogFragment newFragment = new DatePickerFragment(dateEvent);
            newFragment.show(getSupportFragmentManager(), "");
        }
    }

    private void showTimePickerDialog(final boolean hasFocus, final int timeEvent) {
        if (hasFocus) {
            final DialogFragment newFragment = new TimePickerFragment(timeEvent);
            newFragment.show(getSupportFragmentManager(), "");
        }
    }

    private void setUpObservers() {
        viewModel.getErrorMessageRes().observe(this, this::observeErrorMessageRes);
        viewModel.getAskPermission().observe(this, this::observeAskPermission);
        viewModel.getErrorMessageResException().observe(this, this::observeErrorMessageResException);
    }

    private void observeErrorMessageRes(final int errorMessageRes) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.error);
        builder.setMessage(errorMessageRes);
        builder.setPositiveButton(R.string.ok, (dialog, which) -> {});
        builder.show();
    }

    private void observeErrorMessageResException(final Pair<Integer, Exception> pair) {
        final int errorMessageRes = pair.first;
        final Exception exception = pair.second;
        Log.e("Error", getString(errorMessageRes), exception);
        observeErrorMessageRes(errorMessageRes);
    }

    private void observeAskPermission(final AddEventToCalendarModel model) {
        this.model = model;
        validatePermissions();
    }

    @AfterPermissionGranted(READ_WRITE_CALENDAR_PERM)
    public void validatePermissions() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.WRITE_CALENDAR)) {
            // Have permissions, select calendar
            if (calendarHandler != null) {
                calendarHandler.selectCalendar();
            }
        } else {
            // Ask for Write Calendar permission
            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.calendar_permission),
                    READ_WRITE_CALENDAR_PERM,
                    READ_WRITE_CALENDAR);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onCalendarQueryComplete(long calendarId) {
        if (model != null && calendarHandler != null) {
            calendarHandler.addEventToCalendar(model, calendarId);
        }
    }

    @Override
    public void onEventInsertComplete(long eventId) {
        if (model != null && calendarHandler != null && model.getMinutesReminder() > 0) {
            calendarHandler.addReminderToEvent(model, eventId);
        } else {
            startActivity(new Intent(this, FinishedActivity.class));
        }
    }

    @Override
    public void onReminderInsertComplete() {
        startActivity(new Intent(this, FinishedActivity.class));
    }

    @Override
    public void onError(int errorMessageRes) {
        observeErrorMessageRes(errorMessageRes);
    }

    @Override
    public void onError(int errorMessageRes, Exception exception) {
        Log.e("Error", getString(errorMessageRes), exception);
        observeErrorMessageRes(errorMessageRes);
    }
}
