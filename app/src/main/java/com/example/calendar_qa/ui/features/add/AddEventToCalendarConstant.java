package com.example.calendar_qa.ui.features.add;

final class AddEventToCalendarConstant {

    static final transient int START_DATE_EVENT = 1;
    static final transient int END_DATE_EVENT = 2;

    static final transient int START_TIME_EVENT = 10;
    static final transient int END_TIME_EVENT = 20;

    private AddEventToCalendarConstant() throws IllegalAccessException {
        throw new IllegalAccessException("This is a utility class");
    }
}
