package com.example.calendar_qa.ui.features.add;

import androidx.annotation.Nullable;

import com.example.calendar_qa.ui.util.DateUtil;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Objects;

public class AddEventToCalendarModel implements Cloneable {

    private transient String title;
    private transient String location;
    private transient String description;
    private transient String startDate;
    private transient String startTime;
    private transient String endDate;
    private transient String endTime;
    private transient Calendar startDateTime;
    private transient Calendar endDateTime;
    private transient int minutesReminder;
    private transient String minutesReminderText;

    AddEventToCalendarModel() {
        // Empty constructor required
    }

    void setDate(final int type, final int year, final int month, final int day) {
        if (type == AddEventToCalendarConstant.START_DATE_EVENT) {
            this.startDate = DateUtil.getFormattedDate(year, month, day);
        } else if (type == AddEventToCalendarConstant.END_DATE_EVENT) {
            this.endDate = DateUtil.getFormattedDate(year, month, day);
        }
    }

    void setTime(final int type, final int hourOfDay, final int minute) {
        if (type == AddEventToCalendarConstant.START_TIME_EVENT) {
            this.startTime = DateUtil.getFormattedTime(hourOfDay, minute);
        } else if (type == AddEventToCalendarConstant.END_TIME_EVENT) {
            this.endTime = DateUtil.getFormattedTime(hourOfDay, minute);
        }
    }

    boolean hasValidDateTimes() throws ParseException {
        this.startDateTime = DateUtil.parseDateTime(this.startDate, this.startTime);
        this.endDateTime = DateUtil.parseDateTime(this.endDate, this.endTime);
        return endDateTime.after(startDateTime);
    }

    @Override
    public AddEventToCalendarModel clone() throws CloneNotSupportedException {
        final AddEventToCalendarModel model = (AddEventToCalendarModel) super.clone();
        model.startDateTime = (Calendar) this.startDateTime.clone();
        model.endDateTime = (Calendar) this.endDateTime.clone();
        return model;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof AddEventToCalendarModel)) {
            return false;
        } else {
            final AddEventToCalendarModel that = (AddEventToCalendarModel) obj;
            return Objects.equals(this.title, that.title)
                    && Objects.equals(this.location, that.location)
                    && Objects.equals(this.description, that.description)
                    && Objects.equals(this.startDate, that.startDate)
                    && Objects.equals(this.startTime, that.startTime)
                    && Objects.equals(this.endDate, that.endDate)
                    && Objects.equals(this.endTime, that.endTime)
                    && Objects.equals(this.startDateTime, that.startDateTime)
                    && Objects.equals(this.endDateTime, that.endDateTime)
                    && this.minutesReminder == that.minutesReminder
                    && Objects.equals(this.minutesReminderText, that.minutesReminderText);
        }
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash *= 31 + (title == null ? 0 : title.hashCode());
        hash *= 31 + (location == null ? 0 : location.hashCode());
        hash *= 31 + (description == null ? 0 : description.hashCode());
        hash *= 31 + (startDate == null ? 0 : startDate.hashCode());
        hash *= 31 + (startTime == null ? 0 : startTime.hashCode());
        hash *= 31 + (endDate == null ? 0 : endDate.hashCode());
        hash *= 31 + (endTime == null ? 0 : endTime.hashCode());
        hash *= 31 + (startDateTime == null ? 0 : startDateTime.hashCode());
        hash *= 31 + (endDateTime == null ? 0 : endDateTime.hashCode());
        hash *= 31 + (minutesReminder);
        hash *= 31 + (minutesReminderText == null ? 0 : minutesReminderText.hashCode());
        return hash;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public Calendar getStartDateTime() {
        return startDateTime;
    }

    public Calendar getEndDateTime() {
        return endDateTime;
    }

    public int getMinutesReminder() {
        try {
            minutesReminder = Integer.parseInt(this.minutesReminderText);
            return minutesReminder;
        } catch (NumberFormatException exception) {
            minutesReminder = 0;
            return minutesReminder;
        }
    }

    public String getMinutesReminderText() {
        return minutesReminderText;
    }

    public void setMinutesReminderText(String minutesReminderText) {
        this.minutesReminderText = minutesReminderText;
    }
}
