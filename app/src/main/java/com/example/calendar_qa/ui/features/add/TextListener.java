package com.example.calendar_qa.ui.features.add;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import com.example.calendar_qa.R;
import com.google.android.material.textfield.TextInputLayout;

class TextListener implements TextWatcher {

    private final transient TextInputLayout textInputLayout;
    private final transient Listener listener;

    TextListener(final TextInputLayout textInputLayout,
                        final Listener listener) {
        this.textInputLayout = textInputLayout;
        this.listener = listener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        Log.d("AddEventToCalendar", "beforeTextChanged");
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.d("AddEventToCalendar", "onTextChanged");
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length() == 0) {
            listener.onTextInputLayoutError(textInputLayout, R.string.field_empty_error_message);
            listener.hideKeyword();
        } else if (textInputLayout.isCounterEnabled() && s.length() > textInputLayout.getCounterMaxLength()) {
            listener.onTextInputLayoutError(textInputLayout, R.string.field_max_characters_error_message);
        } else {
            textInputLayout.setErrorEnabled(false);
            textInputLayout.setEndIconDrawable(R.drawable.ic_check);
        }
        listener.validateEnablingButton();
    }

    interface Listener {

        void onTextInputLayoutError(TextInputLayout textInputLayout, int errorMessageRes);

        void hideKeyword();

        void validateEnablingButton();
    }
}
