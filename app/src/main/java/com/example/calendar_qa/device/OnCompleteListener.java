package com.example.calendar_qa.device;

public interface OnCompleteListener {

    void onCalendarQueryComplete(long calendarId);

    void onEventInsertComplete(long eventId);

    void onReminderInsertComplete();

    void onError(int errorMessageRes);

    void onError(int errorMessageRes, Exception exception);
}
