package com.example.calendar_qa.device;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;

import com.example.calendar_qa.R;
import com.example.calendar_qa.ui.features.add.AddEventToCalendarModel;

public class CalendarHandler extends AsyncQueryHandler {

    private static final transient int SELECT_CALENDAR_TOKEN = 10;
    private static final transient int ADD_EVENT_TOKEN = 20;
    private static final transient int ADD_REMINDER_TOKEN = 30;

    // Projection array. Creating indices for this array instead of doing
    // dynamic lookups improves performance.
    private static final transient String[] CALENDAR_PROJECTION = new String[] {
            CalendarContract.Calendars._ID,          // 0
    };

    // The indices for the projection array above.
    private static final transient int PROJECTION_ID_INDEX = 0;

    private final transient OnCompleteListener listener;

    public CalendarHandler(final ContentResolver cr, final OnCompleteListener listener) {
        super(cr);
        this.listener = listener;
    }

    public void selectCalendar() {
        startQuery(SELECT_CALENDAR_TOKEN, new Object(), CalendarContract.Calendars.CONTENT_URI,
                CALENDAR_PROJECTION, null, null,
                CalendarContract.Calendars._ID);
    }

    public void addEventToCalendar(final AddEventToCalendarModel model, final long calendarId) {
        final ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.CALENDAR_ID, calendarId);
        values.put(CalendarContract.Events.TITLE, model.getTitle());
        values.put(CalendarContract.Events.EVENT_LOCATION, model.getLocation());
        values.put(CalendarContract.Events.DESCRIPTION, model.getDescription());
        values.put(CalendarContract.Events.DTSTART, model.getStartDateTime().getTimeInMillis());
        values.put(CalendarContract.Events.DTEND, model.getEndDateTime().getTimeInMillis());
        values.put(CalendarContract.Events.EVENT_TIMEZONE, model.getStartDateTime().getTimeZone().getID());
        startInsert(ADD_EVENT_TOKEN, new Object(), CalendarContract.Events.CONTENT_URI, values);
    }

    public void addReminderToEvent(final AddEventToCalendarModel model, final long eventId) {
        final ContentValues values = new ContentValues();
        values.put(CalendarContract.Reminders.MINUTES, model.getMinutesReminder());
        values.put(CalendarContract.Reminders.EVENT_ID, eventId);
        values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        startInsert(ADD_REMINDER_TOKEN, new Object(), CalendarContract.Reminders.CONTENT_URI, values);
    }

    @Override
    protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
        super.onQueryComplete(token, cookie, cursor);
        if (token == SELECT_CALENDAR_TOKEN && cursor != null && cursor.moveToFirst() && listener != null) {
            final long calendarId = cursor.getLong(PROJECTION_ID_INDEX);
            listener.onCalendarQueryComplete(calendarId);
        } else if (listener != null) {
            listener.onError(R.string.calendar_could_not_be_selected);
        }
    }

    @Override
    protected void onInsertComplete(int token, Object cookie, Uri uri) {
        super.onInsertComplete(token, cookie, uri);
        if (token == ADD_EVENT_TOKEN && uri != null && uri.getLastPathSegment() != null && listener != null) {
            try {
                final long eventId = Long.parseLong(uri.getLastPathSegment());
                listener.onEventInsertComplete(eventId);
            } catch (NumberFormatException exception) {
                listener.onError(R.string.event_id_could_not_be_parse, exception);
            }
        } else if (token == ADD_REMINDER_TOKEN && uri != null && uri.getLastPathSegment() != null && listener != null) {
            listener.onReminderInsertComplete();
        } else if (token == ADD_EVENT_TOKEN && listener != null) {
            listener.onError(R.string.event_could_not_be_added);
        } else if (token == ADD_REMINDER_TOKEN && listener != null) {
            listener.onError(R.string.reminder_could_not_be_added);
        }
    }
}
