package com.example.calendar_qa.ui.features.configure;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.IsNot.not;

import com.example.calendar_qa.R;
import com.example.calendar_qa.R.layout;
import com.example.calendar_qa.ui.features.add.AddEventToCalendarActivity;
import com.example.calendar_qa.databinding.ActivityConfigureMyEventBinding;

import com.example.calendar_qa.ui.features.configure.*;

@RunWith(AndroidJUnit4.class)
public class  ConfigureMyEventActivityTest {

    @Rule
    public ActivityTestRule<ConfigureMyEventActivity> activityTestRule =
            new ActivityTestRule<>(ConfigureMyEventActivity.class);

    @Test
    public void testConfigureButtonVisible() {
        onView(withId(R.id.btn_configure)).check(matches( isDisplayed() ));
    }

    @Test
    public void testConfigureButtonEnable() {
        onView(withId(R.id.btn_configure)).check (matches( isEnabled() ));
    }

    @Test
    public void testConfigureButtonClickable() {
        onView(withId(R.id.btn_configure)).check (matches( isClickable() ));
    }
}
