package com.example.calendar_qa.ui.features.finished;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class FinishedActivityTest {

    @Rule
    public ActivityTestRule<FinishedActivity> activityTestRule =
            new ActivityTestRule<>(FinishedActivity.class);

    @Test
    public void testBackToHomeButtonVisible() {
        onView(withId(R.id.btn_back_to_home)).check(matches( isDisplayed() ));
    }

    @Test
    public void testBackToHomeButtonEnable() {
        onView(withId(R.id.btn_back_to_home)).check(matches( isEnabled() )); 
    }

    @Test
    public void testBackToHomeButtonClickable() {
        onView(withId(R.id.btn_back_to_home)).check(matches( isClickable() ));
    }
}
