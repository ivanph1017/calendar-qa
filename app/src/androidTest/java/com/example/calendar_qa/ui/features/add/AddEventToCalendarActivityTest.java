package com.example.calendar_qa.ui.features.add;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import androidx.annotation.IdRes;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.example.calendar_qa.R;
import com.google.android.material.textfield.TextInputLayout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class AddEventToCalendarActivityTest {

    private static final String ANY_STRING = "fdsfdsf";
    private static final String EMPTY_STRING = "";
    private static final String START_DATE = "15/10/2019";
    private static final String START_TIME = "3:00 PM";
    private static final String END_DATE = "16/10/2019";
    private static final String END_TIME = "4:00 PM";

    @Rule
    public ActivityTestRule<AddEventToCalendarActivity> activityTestRule =
            new ActivityTestRule<>(AddEventToCalendarActivity.class);

    @Test
    public void testErrorMessageOnEmptyFields() {
        testSetUp();
        testErrorMessageOnEmptyField(R.id.layout_title, R.id.et_title);
        testErrorMessageOnEmptyField(R.id.layout_location, R.id.et_location);
        testErrorMessageOnEmptyField(R.id.layout_description, R.id.et_description);
    }

    @Test
    public void testErrorMessageOnMaxTextExceededFields() {
        testSetUp();
        testErrorMessageOnMaxTextExceeded(R.id.layout_title, R.id.et_title);
        testErrorMessageOnMaxTextExceeded(R.id.layout_location, R.id.et_location);
        testErrorMessageOnMaxTextExceeded(R.id.layout_description, R.id.et_description);
    }

    @Test
    public void testFilledFields() {
        testSetUp();
        testFilledField(R.id.layout_title, R.id.et_title, ANY_STRING, false);
        testFilledField(R.id.layout_location, R.id.et_location, ANY_STRING, false);
        testFilledField(R.id.layout_description, R.id.et_description, ANY_STRING, false);
        testFilledField(R.id.layout_start_date, R.id.et_start_date, START_DATE, true);
        testFilledField(R.id.layout_start_time, R.id.et_start_time, START_TIME, true);
        testFilledField(R.id.layout_end_date, R.id.et_end_date, END_DATE, true);
        testFilledField(R.id.layout_end_time, R.id.et_end_time, END_TIME, true);

        onView(withId(R.id.btn_add)).check(matches(allOf(isDisplayed(), isEnabled())));
    }

    @Test
    public void testDisablingAddButton() {
        testSetUp();

        final int [] layoutResourceIds = {R.id.layout_title, R.id.layout_location, R.id.layout_description,
                R.id.layout_start_date, R.id.layout_start_time, R.id.layout_end_date, R.id.layout_end_time};
        final int [] editTextResourceIds = {R.id.et_title, R.id.et_location, R.id.et_description,
                R.id.et_start_date, R.id.et_start_time, R.id.et_end_date, R.id.et_end_time};
        final String [] texts = {ANY_STRING, ANY_STRING, ANY_STRING, START_DATE, START_TIME, END_DATE, END_TIME};
        final boolean [] flags = {false, false, false, true, true, true, true};

        final int length= layoutResourceIds.length;

        for (int i = 0; i < length; i++) {
            testFilledField(layoutResourceIds[i], editTextResourceIds[i], texts[i], flags[i]);
        }

        for (int i = 0; i < length; i++) {
            onView(withId(editTextResourceIds[i])).perform(replaceText(EMPTY_STRING));
            onView(withId(R.id.btn_add)).check(matches(allOf(isDisplayed(), not(isEnabled()))));
            testFilledField(layoutResourceIds[i], editTextResourceIds[i], texts[i], flags[i]);
        }
    }

    private void testSetUp() {
        onView(withId(R.id.layout_title)).check(matches(isDisplayed()));
        onView(withId(R.id.et_title)).check(matches(isDisplayed()));

        onView(withId(R.id.layout_location)).check(matches(isDisplayed()));
        onView(withId(R.id.et_location)).check(matches(isDisplayed()));

        onView(withId(R.id.layout_description)).check(matches(isDisplayed()));
        onView(withId(R.id.et_description)).check(matches(isDisplayed()));

        onView(withId(R.id.layout_start_date)).check(matches(isDisplayed()));
        onView(withId(R.id.et_start_date)).check(matches(isDisplayed()));

        onView(withId(R.id.layout_start_time)).check(matches(isDisplayed()));
        onView(withId(R.id.et_start_time)).check(matches(isDisplayed()));

        onView(withId(R.id.layout_end_date)).check(matches(isDisplayed()));
        onView(withId(R.id.et_end_date)).check(matches(isDisplayed()));

        onView(withId(R.id.layout_end_time)).check(matches(isDisplayed()));
        onView(withId(R.id.et_end_time)).check(matches(isDisplayed()));

        onView(withId(R.id.btn_add)).check(matches(allOf(isDisplayed(), not(isEnabled()))));
    }

    private void testErrorMessageOnEmptyField(@IdRes final int layoutResourceId,
                                              @IdRes final int editTextResourceId) {
        onView(withId(editTextResourceId)).perform(click())
                .perform(replaceText(ANY_STRING))
                .perform(replaceText(EMPTY_STRING));
        final TextInputLayout textInputLayout =
                activityTestRule.getActivity().findViewById(layoutResourceId);
        assertTrue(textInputLayout.isErrorEnabled());
        assertTrue(textInputLayout.isEndIconVisible());

        if (textInputLayout.getError() != null) {
            assertEquals(textInputLayout.getError().toString(),
                    activityTestRule.getActivity().getString(R.string.field_empty_error_message));
        }
    }

    private void testErrorMessageOnMaxTextExceeded(@IdRes final int layoutResourceId,
                                                   @IdRes final int editTextResourceId) {

        final TextInputLayout textInputLayout =
                activityTestRule.getActivity().findViewById(layoutResourceId);
        final String textOverMaxLength =
                RandomStringUtils.random(textInputLayout.getCounterMaxLength() + 1, true, true);

        onView(withId(editTextResourceId)).perform(click())
                .perform(replaceText(textOverMaxLength));

        assertTrue(textInputLayout.isErrorEnabled());
        assertTrue(textInputLayout.isEndIconVisible());

        if (textInputLayout.getError() != null) {
            assertEquals(textInputLayout.getError().toString(),
                    activityTestRule.getActivity().getString(R.string.field_max_characters_error_message));
        }
    }

    private void testFilledField(@IdRes final int layoutResourceId,
                                 @IdRes final int editTextResourceId,
                                 final String text, final boolean dateTimeField) {

        final TextInputLayout textInputLayout =
                activityTestRule.getActivity().findViewById(layoutResourceId);

        if (dateTimeField) {
            onView(withId(editTextResourceId)).perform(replaceText(text));
        } else {
            onView(withId(editTextResourceId)).perform(click())
                    .perform(replaceText(text));
        }

        assertFalse(textInputLayout.isErrorEnabled());
        assertTrue(textInputLayout.isEndIconVisible());
    }
}
